<?php

namespace Abetzi\FileLinesIterator;

use PHPUnit\Framework\TestCase;

class FileLinesIteratorTest extends TestCase
{

	/**
	 * @test
	 */
	public function itFailsWhenFileDoesNotExist()
	{
		// Given
		$notExist = 'file-does-not-exist.csv';

		// Expected
		$this->expectException(\InvalidArgumentException::class);

		// When
		FileLinesIterator::getInstance($notExist)->lines();
	}

	/**
	 * @test
	 */
	public function itReadsAllLinesFromFile()
	{
		// Given
		$file = __DIR__ . '/100-lines.csv';

		// When
		$iterator = FileLinesIterator::getInstance($file)->lines();

		$count = 0;
		foreach ($iterator as $line) {
			$count++;
		}

		// Than, last one is empty line
		$this->assertEquals(100, $count);
	}
}
