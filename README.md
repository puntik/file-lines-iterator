## Install 

TBD - something like
````bash
composer install abetzi/file-lines-iterator
````

## Usage
````php
$file  = '/path/to/iterated/file';

$lines = FileLinesIterator::getInstance($file)->lines();

foreach($lines as $line) {
	// do something useful with read line
}

````

## Benchmarks

TBD - compare with usual methods

