<?php declare(strict_types = 1);

namespace Abetzi\FileLinesIterator;

class FileLinesIterator
{

	private const CHUNK_SIZE = 8192;

	/** @var bool|resource */
	private $handle;

	public static function getInstance(string $file): self
	{
		if (! (is_file($file) && is_readable($file))) {
			throw new \InvalidArgumentException(sprintf('File %s not found or not readable.', $file), 404);
		}

		$handle = fopen($file, 'rb');

		return new self($handle);
	}

	private function __construct($handle)
	{
		$this->handle = $handle;
	}

	public function __destruct()
	{
		fclose($this->handle);
	}

	public function lines(): \Traversable
	{
		while (($line = fgets($this->handle, self::CHUNK_SIZE)) !== false) {
			yield $line;
		}
	}
}
